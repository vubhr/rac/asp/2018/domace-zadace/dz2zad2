#include "Adresar.h"
#include <exception>

using std::stoi;

using std::string;
using std::map;
using std::vector;

int Adresar::hashFunkcija(string jmbag) {
	try {
		return stoi(jmbag.substr(jmbag.length() - 1, 1));
	}
	catch (std::invalid_argument e) {
		return -1;
	}
}

void Adresar::dodajStudenta(Student s) {
	auto pretinac = hashFunkcija(s.jmbag);
	adresar[pretinac].push_back(s);
}

Student Adresar::dohvatiStudenta(string jmbag) {
	auto pretinac = hashFunkcija(jmbag);
	for (Student s : adresar[pretinac]) {
		if (s.jmbag == jmbag) {
			return s;
		}
	}
	throw std::exception("Student se ne nalazi u adresaru.");
}