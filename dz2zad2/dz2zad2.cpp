#include <iostream>
#include "Adresar.h"

using std::cout;
using std::endl;

int main() {
	Adresar studenti;
	studenti.dodajStudenta({ "0036347423", "Hrvoje", "Horvat" });
	studenti.dodajStudenta({ "0036736892", "Jelena", "Rozga" });
	studenti.dodajStudenta({ "2401016892", "Nina", "Badric" });

	Student s = studenti.dohvatiStudenta("0036347423");
	try {
		cout << s.jmbag << " " << s.ime << " " << s.prezime << endl;
	}
	catch (...) {
		cout << "Student se ne nalazi u adresaru" << endl;
	}
}
