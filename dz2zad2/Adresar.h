#pragma once

#include <map>
#include <vector>
#include <string>

struct Student {
	std::string jmbag;
	std::string ime;
	std::string prezime;
};

class Adresar {
public:
	void dodajStudenta(Student);
	Student dohvatiStudenta(std::string);
private:
	int hashFunkcija(std::string);
	std::map<int, std::vector<Student>> adresar;
};